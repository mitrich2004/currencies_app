package com.mitrich.android.currunciesapp.data

data class CurrencyRate(
    val currentCurrencyRate: Double,
    val oneMonthAgoCurrencyRate: Double,
    val currencyCode: String
)