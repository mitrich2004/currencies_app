package com.mitrich.android.currunciesapp.data

data class FavoriteCurrencyData(
    val currencyRate: Double,
    val currencyName: String,
    val currencyCode: String
)