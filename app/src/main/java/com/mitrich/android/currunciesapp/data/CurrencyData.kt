package com.mitrich.android.currunciesapp.data

data class CurrencyData(
    val currencyName: String,
    val currencyCode: String
)