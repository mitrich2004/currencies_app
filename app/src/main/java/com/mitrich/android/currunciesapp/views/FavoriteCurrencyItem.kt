package com.mitrich.android.currunciesapp.views

import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.data.FavoriteCurrencyData
import com.mitrich.android.currunciesapp.utils.setRoundedLayout
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.favorite_currency_item.view.*

class FavoriteCurrencyItem(val currency: FavoriteCurrencyData) : Item<ViewHolder>() {

    override fun getLayout() = R.layout.favorite_currency_item

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.favorite_currency_layout.setRoundedLayout()
        viewHolder.itemView.currency_code_text.text = currency.currencyCode
        viewHolder.itemView.currency_name_text.text = currency.currencyName
        viewHolder.itemView.currency_rate_text.text = String.format("%.2f", currency.currencyRate)
    }
}
