package com.mitrich.android.currunciesapp.data

data class UserSettingsData(
    val darkTheme: Boolean,
    val accentColorId: Int
)