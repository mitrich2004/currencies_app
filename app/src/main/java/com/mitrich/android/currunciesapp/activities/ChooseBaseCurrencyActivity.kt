package com.mitrich.android.currunciesapp.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.allSymbolsRequestApi
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.baseCurrencyData
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.darkTheme
import com.mitrich.android.currunciesapp.data.CurrencyData
import com.mitrich.android.currunciesapp.utils.*
import com.mitrich.android.currunciesapp.views.BaseCurrencyItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_all_currencies.search_edit_text
import kotlinx.android.synthetic.main.activity_all_currencies.search_image
import kotlinx.android.synthetic.main.activity_choose_base_currency.*
import kotlinx.android.synthetic.main.activity_currency.back_image
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.*

class ChooseBaseCurrencyActivity : AppCompatActivity() {

    val adapter = GroupAdapter<ViewHolder>()
    private val listOfCurrenciesNamesAndCodes = arrayListOf<CurrencyData>()

    override fun onCreate(savedInstanceState: Bundle?) {

        if (darkTheme) {
            setTheme(R.style.Theme_AppCompat_NoActionBar)
        } else {
            setTheme(R.style.Theme_AppCompat_Light_NoActionBar)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_base_currency)

        choose_base_currency_activity_toolbar.setBackgroundColor()
        loading_circle.setLoadingCircleColor(this)
        back_image.setImageColor(this)
        search_image.setImageColor(this)

        if (darkTheme) {
            choose_base_currency_layout.setDarkLayoutBackground()
        }

        base_currencies_recyclerview.adapter = adapter

        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        adapter.setOnItemClickListener { item, _ ->
            val baseCurrencyItem = item as BaseCurrencyItem
            baseCurrencyData = CurrencyData(
                baseCurrencyItem.currencyData.currencyName,
                baseCurrencyItem.currencyData.currencyCode
            )
            File("$filesDir/${FavoritesActivity.baseCurrencyFileName}").writeText(
                Gson().toJson(
                    baseCurrencyData
                )
            )
            intentSettingActivity()
        }

        back_image.setOnClickListener {
            if (search_edit_text.visibility == View.GONE) {
                intentSettingActivity()
            } else {
                search_edit_text.visibility = View.GONE
                base_currency_text.visibility = View.VISIBLE
                search_edit_text.setText("")
                inputMethodManager.hideSoftInputFromWindow(search_edit_text.windowToken, 0)
                if (!search_edit_text.text.toString().isBlank()) {
                    adapter.clear()
                    listOfCurrenciesNamesAndCodes.clear()
                    loadAllCurrencies()
                }
            }
        }

        search_image.setOnClickListener {
            search_edit_text.visibility = View.VISIBLE
            base_currency_text.visibility = View.GONE
            search_edit_text.requestFocus()
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
        }

        search_edit_text.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, start: Int, before: Int, count: Int) {
                adapter.clear()
                for (currency in listOfCurrenciesNamesAndCodes) {
                    if (currency.currencyCode.toLowerCase(Locale.getDefault())
                            .contains(
                                p0.toString().toLowerCase(Locale.getDefault())
                            ) || currency.currencyName.toLowerCase(Locale.getDefault())
                            .contains(p0.toString().toLowerCase(Locale.getDefault()))
                    ) {
                        adapter.add(
                            BaseCurrencyItem(
                                listOfCurrenciesNamesAndCodes[listOfCurrenciesNamesAndCodes.indexOf(
                                    currency
                                )]
                            )
                        )
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable?) {}
        })

        loadAllCurrencies()
    }

    private fun loadAllCurrencies() {
        val request = Request.Builder().url(allSymbolsRequestApi).build()
        val client = OkHttpClient()

        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()

                try {
                    val currenciesJsonArray = getJsonArrayOfAllCurrencies(JSONObject(body))!!
                    for (index in 0 until currenciesJsonArray.length()) {
                        val currencyJsonObject = currenciesJsonArray.getJSONObject(index)
                        val currencyData = getCurrencyData(currencyJsonObject)
                        listOfCurrenciesNamesAndCodes.add(currencyData!!)

                        runOnUiThread {
                            adapter.add(BaseCurrencyItem(currencyData))
                            loading_circle.visibility = View.GONE
                        }
                    }
                } catch (e: JSONException) {
                    runOnUiThread {
                        base_currency_api_crash_text.visibility = View.VISIBLE
                        loading_circle.visibility = View.GONE
                    }
                }
            }

            override fun onFailure(call: Call, e: IOException) {}
        })
    }

    private fun intentSettingActivity() {
        val settingsActivityIntent = Intent(this, SettingsActivity::class.java)
        settingsActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(settingsActivityIntent)
        finish()
    }

    override fun onBackPressed() {
        intentSettingActivity()
    }
}
