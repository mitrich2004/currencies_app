package com.mitrich.android.currunciesapp.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.accentColorId
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.baseCurrencyData
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.darkTheme
import com.mitrich.android.currunciesapp.data.UserSettingsData
import com.mitrich.android.currunciesapp.utils.*
import kotlinx.android.synthetic.main.activity_favorites.currencies_image
import kotlinx.android.synthetic.main.activity_favorites.favorites_image
import kotlinx.android.synthetic.main.activity_favorites.settings_image
import kotlinx.android.synthetic.main.activity_settings.*
import java.io.File

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        if (darkTheme) {
            setTheme(R.style.Theme_AppCompat_NoActionBar)
        } else {
            setTheme(R.style.Theme_AppCompat_Light_NoActionBar)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val listOfImages = listOf(currencies_image, favorites_image, change_base_currency_image)
        val listOfColorButtons = listOf(
            blue_color_button,
            green_color_button,
            purple_color_button,
            rubin_color_button,
            sea_color_button
        )

        if (darkTheme) {
            settings_layout.setDarkLayoutBackground()
            dark_theme_button.isChecked = true
            dark_theme_button.setAccentColor(this)
            light_theme_button.setGrayColor(this)
        } else {
            light_theme_button.isChecked = true
            light_theme_button.setAccentColor(this)
            dark_theme_button.setGrayColor(this)
        }

        base_currency_layout.setRoundedLayout()
        listOfImages.forEach {
            it.setImageColor(this)
        }

        bottom_navigation_bar.setBackgroundColor()
        app_theme_layout.setRoundedLayout()
        color_theme_layout.setRoundedLayout()
        settings_image.setAccentColor(this)

        when (accentColorId) {
            R.color.blue_accent -> blue_color_button.isChecked = true
            R.color.green_accent -> green_color_button.isChecked = true
            R.color.purple_accent -> purple_color_button.isChecked = true
            R.color.rubin_accent -> rubin_color_button.isChecked = true
            R.color.sea_accent -> sea_color_button.isChecked = true
        }

        listOfColorButtons.forEach {
            if (it.isChecked) {
                it.setAccentColor(this)
            } else {
                it.setGrayColor(this)
            }
        }

        favorites_image.setOnClickListener {
            val favoritesActivityIntent = Intent(this, FavoritesActivity::class.java)
            favoritesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(favoritesActivityIntent)
        }

        currencies_image.setOnClickListener {
            val allCurrenciesActivityIntent = Intent(this, AllCurrenciesActivity::class.java)
            allCurrenciesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(allCurrenciesActivityIntent)
        }

        base_currency_layout.setOnClickListener {
            val chooseBaseCurrencyActivityIntent =
                Intent(this, ChooseBaseCurrencyActivity::class.java)
            startActivity(chooseBaseCurrencyActivityIntent)
        }

        dark_theme_button.setOnClickListener {
            if (!darkTheme) {
                darkTheme = true
                light_theme_button.isChecked = false
                applyChanges()
            }
        }

        light_theme_button.setOnClickListener {
            if (darkTheme) {
                darkTheme = false
                light_theme_button.isChecked = true
                applyChanges()
            }
        }

        blue_color_button.setOnClickListener {
            if (!blue_color_button.isChecked) {
                accentColorId = R.color.blue_accent
                blue_color_button.setAccentColor(this)
                applyChanges()
            }
        }

        listOfColorButtons.forEach { button ->
            button.setOnClickListener {
                if (button.isChecked) {
                    accentColorId = when (button) {
                        blue_color_button -> R.color.blue_accent
                        green_color_button -> R.color.green_accent
                        purple_color_button -> R.color.purple_accent
                        rubin_color_button -> R.color.rubin_accent
                        sea_color_button -> R.color.sea_accent
                        else -> R.color.blue_accent
                    }
                    button.setAccentColor(this)
                    applyChanges()
                }
            }
        }

        base_currency_code_text.text = baseCurrencyData.currencyCode
        base_currency_name_text.text = baseCurrencyData.currencyName
    }

    private fun applyChanges() {
        val userSettingsData = Gson().toJson(UserSettingsData(darkTheme, accentColorId))
        File("$filesDir/${FavoritesActivity.userSettingsFileName}").writeText(userSettingsData)
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(0, 0)
    }

    override fun onBackPressed() {
        val favoritesActivityIntent = Intent(this, FavoritesActivity::class.java)
        favoritesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(favoritesActivityIntent)
    }
}
