package com.mitrich.android.currunciesapp.views

import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.data.CurrencyData
import com.mitrich.android.currunciesapp.utils.setRoundedLayout
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.base_currency_item.view.*

class BaseCurrencyItem(
    val currencyData: CurrencyData
) : Item<ViewHolder>() {

    override fun getLayout() = R.layout.base_currency_item

    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.base_currency_layout.setRoundedLayout()
        viewHolder.itemView.base_currency_name_text.text = currencyData.currencyName
        viewHolder.itemView.base_currency_code_text.text = currencyData.currencyCode
    }
}
