package com.mitrich.android.currunciesapp.views

import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.data.CurrencyRate
import com.mitrich.android.currunciesapp.utils.setRoundedLayout
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.currency_rate_item.view.*

class CurrencyRateItem(
    private val currencyRate: CurrencyRate
) : Item<ViewHolder>() {

    override fun getLayout() = R.layout.currency_rate_item

    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.currency_rate_item_layout.setRoundedLayout()
        viewHolder.itemView.currency_code_text.text = currencyRate.currencyCode
        viewHolder.itemView.currency_rate_text.text =
            String.format("%.3f", currencyRate.currentCurrencyRate)

        val currentRate = currencyRate.currentCurrencyRate
        val oneMonthAgoRate = currencyRate.oneMonthAgoCurrencyRate

        when {
            currentRate > oneMonthAgoRate -> {
                viewHolder.itemView.progress_image.setImageResource(R.drawable.ic_up)
            }
            currentRate < oneMonthAgoRate -> {
                viewHolder.itemView.progress_image.setImageResource(R.drawable.ic_down)
            }
            else -> {
                viewHolder.itemView.progress_image.setImageResource(R.drawable.ic_equal)
            }
        }
    }
}
