package com.mitrich.android.currunciesapp.utils

import android.annotation.SuppressLint
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

@SuppressLint("SimpleDateFormat")
fun getHistoricalDateByNumberOfDays(numberOfDays: Int): String {
    val oneMonthAgoTimestamp =
        TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - 86400 * (numberOfDays - 1)
    val oneMonthAgoDate = Date(oneMonthAgoTimestamp * 1000L)
    val dateFormatter = SimpleDateFormat("yyyy-MM-dd")
    return dateFormatter.format(oneMonthAgoDate).toString()
}

@SuppressLint("SimpleDateFormat")
fun getCurrentDate(): String {
    val currentTimestamp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
    val currentDate = Date(currentTimestamp * 1000L)
    val dateFormatter = SimpleDateFormat("yyyy-MM-dd")
    return dateFormatter.format(currentDate).toString()
}
