package com.mitrich.android.currunciesapp.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.allSymbolsRequestApi
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.darkTheme
import com.mitrich.android.currunciesapp.data.CurrencyData
import com.mitrich.android.currunciesapp.utils.*
import com.mitrich.android.currunciesapp.views.CurrencyItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_all_currencies.*
import kotlinx.android.synthetic.main.activity_all_currencies.bottom_navigation_bar
import kotlinx.android.synthetic.main.activity_all_currencies.loading_circle
import kotlinx.android.synthetic.main.activity_all_currencies.settings_image
import kotlinx.android.synthetic.main.activity_favorites.currencies_image
import kotlinx.android.synthetic.main.activity_favorites.favorites_image
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

class AllCurrenciesActivity : AppCompatActivity() {

    private val adapter = GroupAdapter<ViewHolder>()
    private val listOfCurrenciesNamesAndCodes = arrayListOf<CurrencyData>()

    override fun onCreate(savedInstanceState: Bundle?) {

        if (darkTheme) {
            setTheme(R.style.Theme_AppCompat_NoActionBar)
        } else {
            setTheme(R.style.Theme_AppCompat_Light_NoActionBar)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_currencies)

        val listOfImages = listOf(search_image, favorites_image, settings_image)

        currencies_activity_toolbar.setBackgroundColor()
        bottom_navigation_bar.setBackgroundColor()
        loading_circle.setLoadingCircleColor(this)
        currencies_image.setAccentColor(this)

        currencies_recyclerview.adapter = adapter

        listOfImages.forEach {
            it.setImageColor(this)
        }

        if (darkTheme) {
            all_currencies_activity_layout.setDarkLayoutBackground()
        }

        favorites_image.setOnClickListener {
            val favoritesActivityIntent = Intent(this, FavoritesActivity::class.java)
            favoritesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(favoritesActivityIntent)
        }

        settings_image.setOnClickListener {
            val settingsActivityIntent = Intent(this, SettingsActivity::class.java)
            settingsActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(settingsActivityIntent)
        }

        adapter.setOnItemClickListener { item, _ ->
            val currencyItem = item as CurrencyItem
            val currencyActivityIntent = Intent(this, CurrencyActivity::class.java)
            currencyActivityIntent.putExtra("currencyCode", currencyItem.currencyData.currencyCode)
            currencyActivityIntent.putExtra("currencyName", currencyItem.currencyData.currencyName)
            startActivity(currencyActivityIntent)
        }

        search_edit_text.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                adapter.clear()
                for (currency in listOfCurrenciesNamesAndCodes) {
                    if (currency.currencyCode.toLowerCase(Locale.getDefault())
                            .contains(
                                p0.toString().toLowerCase(Locale.getDefault())
                            ) || currency.currencyName.toLowerCase(Locale.getDefault())
                            .contains(p0.toString().toLowerCase(Locale.getDefault()))
                    ) {
                        adapter.add(
                            CurrencyItem(
                                listOfCurrenciesNamesAndCodes[listOfCurrenciesNamesAndCodes.indexOf(
                                    currency
                                )], this@AllCurrenciesActivity, filesDir
                            )
                        )
                    }
                }
            }

            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        loadAllCurrencies()
    }

    private fun loadAllCurrencies() {
        val request = Request.Builder().url(allSymbolsRequestApi).build()
        val client = OkHttpClient()

        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()

                try {
                    val currenciesJsonArray = getJsonArrayOfAllCurrencies(JSONObject(body))!!
                    for (index in 0 until currenciesJsonArray.length()) {
                        val currencyJsonObject = currenciesJsonArray.getJSONObject(index)
                        val currencyData = getCurrencyData(currencyJsonObject)
                        listOfCurrenciesNamesAndCodes.add(currencyData!!)
                        runOnUiThread {
                            adapter.add(
                                CurrencyItem(
                                    currencyData,
                                    this@AllCurrenciesActivity,
                                    filesDir
                                )
                            )
                            loading_circle.visibility = View.GONE
                        }
                    }
                } catch (e: JSONException) {
                    runOnUiThread {
                        all_currencies_api_crash_text.visibility = View.VISIBLE
                        loading_circle.visibility = View.GONE
                    }
                }
            }

            override fun onFailure(call: Call, e: IOException) {}
        })
    }

    override fun onBackPressed() {
        val favoritesActivityIntent = Intent(this, FavoritesActivity::class.java)
        favoritesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(favoritesActivityIntent)
    }
}
