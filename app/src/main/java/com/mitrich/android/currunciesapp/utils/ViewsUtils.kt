package com.mitrich.android.currunciesapp.utils

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RadioButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.accentColorId
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.darkTheme

fun ImageView.setAccentColor(context: Context) {
    setColorFilter(ContextCompat.getColor(context, accentColorId), PorterDuff.Mode.SRC_IN)
}

fun ImageView.setImageColor(context: Context) {
    if (darkTheme) {
        setColorFilter(ContextCompat.getColor(context, R.color.light_gray), PorterDuff.Mode.SRC_IN)
    } else {
        setColorFilter(ContextCompat.getColor(context, R.color.dark_gray), PorterDuff.Mode.SRC_IN)
    }
}

fun Button.setWhiteTextColor(context: Context) {
    setTextColor(ContextCompat.getColor(context, android.R.color.white))
}

fun Button.setDefaultTextColor(context: Context) {
    if (darkTheme) {
        setTextColor(ContextCompat.getColor(context, R.color.light_gray))
    } else {
        setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text))
    }
}

fun Button.setRoundedBackground() {
    if (darkTheme) {
        setBackgroundResource(R.drawable.dark_button_background)
    } else {
        setBackgroundResource(R.drawable.light_button_background)
    }
}

fun Button.setActiveRoundedLayout() {
    when (accentColorId) {
        R.color.blue_accent -> setBackgroundResource(R.drawable.blue_button_background)
        R.color.green_accent -> setBackgroundResource(R.drawable.green_button_background)
        R.color.purple_accent -> setBackgroundResource(R.drawable.purple_button_background)
        R.color.rubin_accent -> setBackgroundResource(R.drawable.rubin_button_background)
        R.color.sea_accent -> setBackgroundResource(R.drawable.sea_button_background)
    }
}

fun ConstraintLayout.setRoundedLayout() {
    if (darkTheme) {
        setBackgroundResource(R.drawable.dark_rounded_layout_background)
    } else {
        setBackgroundResource(R.drawable.light_rounded_layout_background)
    }
}

fun ConstraintLayout.setDarkLayoutBackground() {
    setBackgroundResource(R.color.dark_layout_background)
}

fun View.setBackgroundColor() {
    if (darkTheme) {
        setBackgroundResource(R.color.dark)
    } else {
        setBackgroundResource(R.color.light)
    }
}

fun RadioButton.setAccentColor(context: Context) {
    buttonTintList =
        (ColorStateList.valueOf(ContextCompat.getColor(context, accentColorId)))
}

fun RadioButton.setGrayColor(context: Context) {
    buttonTintList = if (darkTheme) {
        (ColorStateList.valueOf(ContextCompat.getColor(context, R.color.light_gray)))
    } else {
        (ColorStateList.valueOf(ContextCompat.getColor(context, R.color.dark_gray)))
    }
}

@Suppress("DEPRECATION")
fun ProgressBar.setLoadingCircleColor(context: Context) {
    indeterminateDrawable.setColorFilter(
        ContextCompat.getColor(
            context,
            accentColorId
        ), PorterDuff.Mode.SRC_IN
    )
}
