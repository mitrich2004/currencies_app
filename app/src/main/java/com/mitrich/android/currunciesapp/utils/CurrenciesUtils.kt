package com.mitrich.android.currunciesapp.utils

import android.util.Log
import com.mitrich.android.currunciesapp.data.CurrencyData
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

fun getCurrentCurrencyRate(currencyCode: String, currencyJsonObject: JSONObject): Double? {
    return try {
        val rates = currencyJsonObject.getJSONObject("rates")
        rates.getDouble(currencyCode)
    } catch (e: JSONException) {
        null
    }
}

fun getHistoricalCurrencyRate(jsonObject: JSONObject, currencyCode: String): Double? {
    return try {
        jsonObject.getDouble(currencyCode)
    } catch (e: JSONException) {
        null
    }
}

fun getCurrencyData(currencyJsonObject: JSONObject): CurrencyData? {
    return try {
        val currencyName = currencyJsonObject.getString("description")
        val currencyCode = currencyJsonObject.getString("code")
        CurrencyData(currencyName, currencyCode)
    } catch (e: JSONException) {
        null
    }
}

fun getCurrencyRates(jsonObject: JSONObject): JSONArray? {
    return try {
        val jsonObjectOfRates = jsonObject.getJSONObject("rates")
        val jsonArrayOfRates =
            getJsonArrayFormJsonObject(jsonObjectOfRates)
        jsonArrayOfRates
    } catch (e: JSONException) {
        null
    }
}

fun getJsonArrayOfAllCurrencies(jsonObject: JSONObject): JSONArray? {
    return try {
        val jsonObjectOfRates = jsonObject.getJSONObject("symbols")
        getJsonArrayFormJsonObject(jsonObjectOfRates)
    } catch (e: JSONException) {
        null
    }
}

fun JSONObject.getCurrencyCode(): String {
    return getString("code")
}

fun JSONObject.getCurrencyName(): String {
    return getString("name")
}

fun JSONArray.addCurrencyDataJsonObject(currencyData: CurrencyData): JSONArray {
    val currencyDataJsonObject = JSONObject()
    currencyDataJsonObject.put("code", currencyData.currencyCode)
    currencyDataJsonObject.put("name", currencyData.currencyName)
    return put(currencyDataJsonObject)
}
