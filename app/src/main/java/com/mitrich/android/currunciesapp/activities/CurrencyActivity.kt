package com.mitrich.android.currunciesapp.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.accentColorId
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.baseCurrencyData
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.darkTheme
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.listOfFavoriteCurrencies
import com.mitrich.android.currunciesapp.data.CurrencyData
import com.mitrich.android.currunciesapp.data.CurrencyRate
import com.mitrich.android.currunciesapp.utils.*
import com.mitrich.android.currunciesapp.views.CurrencyRateItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_currency.*
import okhttp3.*
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.math.RoundingMode
import java.util.*
import kotlin.collections.ArrayList

class CurrencyActivity : AppCompatActivity() {

    private val adapter = GroupAdapter<ViewHolder>()
    private val listOfCurrenciesCodes = arrayListOf(baseCurrencyData.currencyCode, "USD", "EUR")

    override fun onCreate(savedInstanceState: Bundle?) {

        if (darkTheme) {
            setTheme(R.style.Theme_AppCompat_NoActionBar)
        } else {
            setTheme(R.style.Theme_AppCompat_Light_NoActionBar)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency)

        val listOfDateRangeButtons =
            listOf<Button>(thirty_days_button, sixty_days_button, ninety_days_button)

        back_image.setImageColor(this)
        currency_activity_toolbar.setBackgroundColor()
        loading_circle.setLoadingCircleColor(this)

        if (darkTheme) {
            currency_activity_layout.setDarkLayoutBackground()
        }

        val currencyCode = intent.getStringExtra("currencyCode")!!
        val currencyName = intent.getStringExtra("currencyName")!!
        var currencyIsFavorite = false
        thirty_days_button.isEnabled = false
        currency_name_text.text = currencyName
        currency_rates_recyclerview.adapter = adapter

        listOfDateRangeButtons.forEach {
            if (it != thirty_days_button) {
                it.setRoundedBackground()
                it.setRoundedBackground()
                it.setDefaultTextColor(this)
            } else {
                it.setActiveRoundedLayout()
            }
            it.visibility = View.GONE
        }

        if (baseCurrencyData.currencyCode == "USD") {
            listOfCurrenciesCodes.remove("USD")
        } else if (baseCurrencyData.currencyCode == "EUR") {
            listOfCurrenciesCodes.remove("EUR")
        }

        for (index in 0 until listOfFavoriteCurrencies.length()) {
            val jsonCurrencyCode = listOfFavoriteCurrencies.getJSONObject(index).getCurrencyCode()
            currencyIsFavorite = jsonCurrencyCode == currencyCode
            if (currencyIsFavorite) {
                break
            }
        }

        if (currencyIsFavorite) {
            favorite_image.setImageResource(R.drawable.ic_favorites)
            favorite_image.setAccentColor(this)
        } else {
            favorite_image.setImageResource(R.drawable.ic_not_favorite)
            favorite_image.setImageColor(this)
        }

        listOfDateRangeButtons.forEach { button ->
            button.setOnClickListener { clicked ->
                val clickedButton = clicked as Button
                listOfDateRangeButtons.forEach {
                    it.apply {
                        if (clickedButton.id == id) {
                            isEnabled = false
                            setWhiteTextColor(this@CurrencyActivity)
                            setActiveRoundedLayout()
                        } else {
                            setDefaultTextColor(this@CurrencyActivity)
                            setRoundedBackground()
                            isEnabled = true
                        }
                    }
                }
                when (clickedButton.id) {
                    thirty_days_button.id -> prepareDataForLineChart(currencyCode, 30)
                    sixty_days_button.id -> prepareDataForLineChart(currencyCode, 60)
                    ninety_days_button.id -> prepareDataForLineChart(currencyCode, 90)
                }
            }
        }

        favorite_image.setOnClickListener {
            currencyIsFavorite = !currencyIsFavorite
            if (currencyIsFavorite) {
                favorite_image.setImageResource(R.drawable.ic_favorites)
                favorite_image.setAccentColor(this)
                listOfFavoriteCurrencies = listOfFavoriteCurrencies.addCurrencyDataJsonObject(
                    CurrencyData(currencyName, currencyCode)
                )
                File("$filesDir/${FavoritesActivity.favoriteCurrenciesListFileName}").writeText(
                    listOfFavoriteCurrencies.toString()
                )
            } else {
                favorite_image.setImageResource(R.drawable.ic_not_favorite)
                favorite_image.setImageColor(this)
                for (index in 0 until listOfFavoriteCurrencies.length()) {
                    val jsonCurrencyCode =
                        listOfFavoriteCurrencies.getJSONObject(index).getCurrencyCode()
                    if (jsonCurrencyCode == currencyCode) {
                        listOfFavoriteCurrencies.remove(index)
                        File("$filesDir/${FavoritesActivity.favoriteCurrenciesListFileName}").writeText(
                            listOfFavoriteCurrencies.toString()
                        )
                        break
                    }
                }
            }
        }

        back_image.setOnClickListener {
            val favoritesActivityIntent = Intent(this, FavoritesActivity::class.java)
            favoritesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(favoritesActivityIntent)
            finish()
        }

        addCurrencyRateItems(currencyCode)
    }

    private fun addCurrencyRateItems(currencyCode: String) {
        val request = Request.Builder()
            .url("https://api.exchangerate.host/latest?base=$currencyCode&symbols=${baseCurrencyData.currencyCode},USD,EUR")
            .build()
        val client = OkHttpClient()

        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()

                val oneMonthAgoDate = getHistoricalDateByNumberOfDays(30)

                val oneMonthAgoRequest = Request.Builder()
                    .url("https://api.exchangerate.host/$oneMonthAgoDate?base=$currencyCode&symbols=${baseCurrencyData.currencyCode},USD,EUR")
                    .build()

                val oneMonthAgoClient = OkHttpClient()

                oneMonthAgoClient.newCall(oneMonthAgoRequest).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Log.d("ERROR", e.message.toString())
                    }

                    override fun onResponse(call: Call, response: Response) {
                        val oneMonthAgoResponseBody = response.body!!.string()
                        response.close()

                        listOfCurrenciesCodes.forEach {
                            if (it != currencyCode) {
                                try {
                                    val currentCurrencyRate =
                                        getCurrentCurrencyRate(it, JSONObject(body))!!

                                    val oneMonthAgoCurrencyRate = getCurrentCurrencyRate(
                                        it, JSONObject(
                                            oneMonthAgoResponseBody
                                        )
                                    )!!

                                    val currencyRate = CurrencyRate(
                                        currentCurrencyRate,
                                        oneMonthAgoCurrencyRate,
                                        it
                                    )

                                    runOnUiThread {
                                        prepareDataForLineChart(currencyCode, 30)
                                        adapter.add(CurrencyRateItem(currencyRate))
                                        loading_circle.visibility = View.GONE
                                    }
                                } catch (e: NullPointerException) {
                                    runOnUiThread {
                                        currency_api_crash_text.visibility = View.VISIBLE
                                        currency_rates_recyclerview.visibility = View.GONE
                                        loading_circle.visibility = View.GONE
                                    }
                                }
                            }
                        }
                    }
                })
            }

            override fun onFailure(call: Call, e: IOException) {}
        })
    }

    private fun prepareDataForLineChart(currencyCode: String, dateRange: Int) {
        val listOfDateRangeButtons =
            listOf<Button>(thirty_days_button, sixty_days_button, ninety_days_button)

        var compareCurrencyCode = baseCurrencyData.currencyCode

        if (compareCurrencyCode == currencyCode) {
            compareCurrencyCode = if (currencyCode != "USD") "USD" else "EUR"
        }

        val request = Request.Builder()
            .url(
                "https://api.exchangerate.host/timeseries?start_date=${
                    getHistoricalDateByNumberOfDays(
                        dateRange
                    )
                }&end_date=${getCurrentDate()}&base=$currencyCode&symbols=$compareCurrencyCode"
            )
            .build()

        val client = OkHttpClient()

        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()

                try {
                    val jsonArrayOfRates = getCurrencyRates(JSONObject(body))
                    val xAxes = ArrayList<String>()
                    val yAxes = ArrayList<Entry>()
                    for (index in 0 until jsonArrayOfRates!!.length()) {
                        if (baseCurrencyData.currencyCode == jsonArrayOfRates.getJSONObject(index).keys().next()) {
                            val rate = getHistoricalCurrencyRate(
                                jsonArrayOfRates.getJSONObject(index),
                                compareCurrencyCode
                            )!!
                            xAxes.add(index.toString())
                            yAxes.add(Entry(rate.toFloat(), index))
                        }

                    }
                    runOnUiThread {
                        drawLineChart(xAxes, yAxes)
                        line_chart.visibility = View.VISIBLE
                        listOfDateRangeButtons.forEach { button ->
                            button.visibility = View.VISIBLE
                        }
                    }
                } catch (e: NullPointerException) {
                    runOnUiThread {
                        currency_api_crash_text.visibility = View.VISIBLE
                        line_chart.visibility = View.GONE
                        listOfDateRangeButtons.forEach { button ->
                            button.visibility = View.GONE
                        }
                    }
                }
            }

            override fun onFailure(call: Call, e: IOException) {}
        })
    }

    private fun drawLineChart(xAxes: ArrayList<String>, yAxes: ArrayList<Entry>) {
        val lineChart = findViewById<LineChart>(R.id.line_chart)
        val lineDataSet = LineDataSet(yAxes, "")

        if (darkTheme) {
            lineChart.axisLeft.textColor = Color.LTGRAY
        }

        lineDataSet.apply {
            color = ContextCompat.getColor(this@CurrencyActivity, accentColorId)
            setCircleColor(ContextCompat.getColor(this@CurrencyActivity, accentColorId))
            lineWidth = 3f
            setDrawFilled(true)
            fillColor = ContextCompat.getColor(this@CurrencyActivity, accentColorId)
            setDrawCircleHole(false)
        }

        lineChart.apply {
            axisLeft.granularity = 0.0001f
            animateX(2000)
            animateY(500)
            data = LineData(xAxes, lineDataSet)
            setTouchEnabled(false)
            data.setDrawValues(false)
            legend.isEnabled = false
            axisRight.isEnabled = false
            axisLeft.setValueFormatter { value, _ ->
                value.toBigDecimal().setScale(2, RoundingMode.UP).toString()
            }
            xAxis.isEnabled = true
            xAxis.setDrawLabels(false)
            lineChart.setDrawBorders(true)
            setDescription("")
            setNoDataTextDescription("")
        }
    }

    override fun onBackPressed() {
        val favoritesActivityIntent = Intent(this, FavoritesActivity::class.java)
        favoritesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(favoritesActivityIntent)
    }
}
