package com.mitrich.android.currunciesapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.data.CurrencyData
import com.mitrich.android.currunciesapp.data.FavoriteCurrencyData
import com.mitrich.android.currunciesapp.data.UserSettingsData
import com.mitrich.android.currunciesapp.utils.*
import com.mitrich.android.currunciesapp.views.FavoriteCurrencyItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_favorites.*
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.IOException
import kotlin.system.exitProcess

class FavoritesActivity : AppCompatActivity() {

    companion object {
        var baseCurrencyData = CurrencyData("United States Dollar", "USD")
        var listOfFavoriteCurrencies = JSONArray()
        var darkTheme = false
        var accentColorId = R.color.blue_accent
        const val userSettingsFileName = "userSettings.json"
        const val favoriteCurrenciesListFileName = "favoriteCurrencies.json"
        const val baseCurrencyFileName = "baseCurrency.json"
        const val allSymbolsRequestApi = "https://api.exchangerate.host/symbols"
    }

    private val adapter = GroupAdapter<ViewHolder>()
    private val listOfFavoriteCurrenciesCodes = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {

        if (File("$filesDir/$favoriteCurrenciesListFileName").exists()) {
            File("$filesDir/$favoriteCurrenciesListFileName").forEachLine {
                listOfFavoriteCurrencies = JSONArray(it)
            }
        } else {
            listOfFavoriteCurrencies.apply {
                put(JSONObject().put("code", "USD").put("name", "United States Dollar"))
                put(JSONObject().put("code", "EUR").put("name", "Euro"))
            }
            File("$filesDir/$favoriteCurrenciesListFileName").createNewFile()
            File("$filesDir/$favoriteCurrenciesListFileName").writeText(listOfFavoriteCurrencies.toString())
        }

        if (File("$filesDir/$baseCurrencyFileName").exists()) {
            File("$filesDir/$baseCurrencyFileName").forEachLine {
                baseCurrencyData = Gson().fromJson(it, CurrencyData::class.java)
            }
        } else {
            File("$filesDir/$baseCurrencyFileName").createNewFile()
            File("$filesDir/$baseCurrencyFileName").writeText(Gson().toJson(baseCurrencyData))
        }

        if (File("$filesDir/$userSettingsFileName").exists()) {
            File("$filesDir/$userSettingsFileName").forEachLine {
                val userSettingsData = Gson().fromJson(it, UserSettingsData::class.java)
                darkTheme = userSettingsData.darkTheme
                accentColorId = userSettingsData.accentColorId
            }
        } else {
            val userSettingsData = Gson().toJson(UserSettingsData(false, R.color.blue_accent))
            File("$filesDir/$userSettingsFileName").createNewFile()
            File("$filesDir/$userSettingsFileName").writeText(userSettingsData)
        }

        if (darkTheme) {
            setTheme(R.style.Theme_AppCompat_NoActionBar)
        } else {
            setTheme(R.style.Theme_AppCompat_Light_NoActionBar)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites)

        bottom_navigation_bar.setBackgroundColor()
        loading_circle.setLoadingCircleColor(this)
        favorites_image.setAccentColor(this)
        if (darkTheme) favorites_activity_layout.setDarkLayoutBackground()
        currencies_image.setImageColor(this)
        settings_image.setImageColor(this)
        swipe_refresh_layout.setColorSchemeResources(accentColorId, accentColorId, accentColorId)

        favorite_currencies_recyclerview.adapter = adapter

        for (index in 0 until listOfFavoriteCurrencies.length()) {
            val currencyCode = listOfFavoriteCurrencies.getJSONObject(index).getCurrencyCode()
            listOfFavoriteCurrenciesCodes.add(currencyCode)
        }

        currencies_image.setOnClickListener {
            val allCurrenciesActivityIntent = Intent(this, AllCurrenciesActivity::class.java)
            allCurrenciesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(allCurrenciesActivityIntent)
        }

        settings_image.setOnClickListener {
            val settingsActivityIntent = Intent(this, SettingsActivity::class.java)
            settingsActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(settingsActivityIntent)
        }

        adapter.setOnItemClickListener { item, _ ->
            val currencyItem = item as FavoriteCurrencyItem
            val currencyActivityIntent = Intent(this, CurrencyActivity::class.java)
            currencyActivityIntent.putExtra("currencyCode", currencyItem.currency.currencyCode)
            currencyActivityIntent.putExtra("currencyName", currencyItem.currency.currencyName)
            startActivity(currencyActivityIntent)
        }

        swipe_refresh_layout.setOnRefreshListener {
            adapter.clear()
            addFavoriteCurrencyItems(
                baseCurrencyData.currencyCode,
                listOfFavoriteCurrenciesCodes.joinToString(",")
            )
        }

        if (listOfFavoriteCurrencies.length() == 0) {
            no_favorite_currencies_text.visibility = View.VISIBLE
            loading_circle.visibility = View.GONE
        } else {
            addFavoriteCurrencyItems(
                baseCurrencyData.currencyCode,
                listOfFavoriteCurrenciesCodes.joinToString(",")
            )
            no_favorite_currencies_text.visibility = View.GONE
        }
    }

    private fun addFavoriteCurrencyItems(baseCurrencyCode: String, currencyCodes: String) {
        val request = Request.Builder()
            .url("https://api.exchangerate.host/latest?base=$baseCurrencyCode&symbols=$currencyCodes")
            .build()

        val client = OkHttpClient()

        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()

                try {
                    for (index in 0 until listOfFavoriteCurrencies.length()) {
                        val currencyCode =
                            listOfFavoriteCurrencies.getJSONObject(index).getCurrencyCode()
                        val currencyName =
                            listOfFavoriteCurrencies.getJSONObject(index).getCurrencyName()
                        val currencyRate = getCurrentCurrencyRate(currencyCode, JSONObject(body))!!
                        val currency =
                            FavoriteCurrencyData(1 / currencyRate, currencyName, currencyCode)
                        runOnUiThread {
                            adapter.add(FavoriteCurrencyItem(currency))
                            loading_circle.visibility = View.GONE
                            swipe_refresh_layout.isRefreshing = false
                        }
                    }
                } catch (e: NullPointerException) {
                    runOnUiThread {
                        favorites_api_crash_text.visibility = View.VISIBLE
                        loading_circle.visibility = View.GONE
                        swipe_refresh_layout.isRefreshing = false
                    }
                }
            }

            override fun onFailure(call: Call, e: IOException) {}
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        exitProcess(1)
    }
}
