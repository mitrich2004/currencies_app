package com.mitrich.android.currunciesapp.views

import android.content.Context
import com.mitrich.android.currunciesapp.R
import com.mitrich.android.currunciesapp.activities.FavoritesActivity
import com.mitrich.android.currunciesapp.activities.FavoritesActivity.Companion.listOfFavoriteCurrencies
import com.mitrich.android.currunciesapp.data.CurrencyData
import com.mitrich.android.currunciesapp.utils.*
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.currency_item.view.*
import java.io.File

class CurrencyItem(
    val currencyData: CurrencyData,
    private val context: Context,
    private val filesDir: File
) : Item<ViewHolder>() {
    override fun getLayout() = R.layout.currency_item

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.currency_name_text.text = currencyData.currencyName
        viewHolder.itemView.currency_code_text.text = currencyData.currencyCode
        viewHolder.itemView.currency_layout.setRoundedLayout()

        var currencyIsFavorite = false

        for (index in 0 until listOfFavoriteCurrencies.length()) {
            val currencyCode = listOfFavoriteCurrencies.getJSONObject(index).getCurrencyCode()
            currencyIsFavorite = currencyCode == currencyData.currencyCode
            if (currencyIsFavorite) break
        }

        if (currencyIsFavorite) {
            viewHolder.itemView.favorite_image.setImageResource(R.drawable.ic_favorites)
            viewHolder.itemView.favorite_image.setAccentColor(context)
        } else {
            viewHolder.itemView.favorite_image.setImageResource(R.drawable.ic_not_favorite)
            viewHolder.itemView.favorite_image.setImageColor(context)
        }

        viewHolder.itemView.favorite_image.setOnClickListener {
            currencyIsFavorite = !currencyIsFavorite
            if (currencyIsFavorite) {
                viewHolder.itemView.favorite_image.setImageResource(R.drawable.ic_favorites)
                viewHolder.itemView.favorite_image.setAccentColor(context)
                listOfFavoriteCurrencies =
                    listOfFavoriteCurrencies.addCurrencyDataJsonObject(currencyData)
                File("$filesDir/${FavoritesActivity.favoriteCurrenciesListFileName}").writeText(
                    listOfFavoriteCurrencies.toString()
                )
            } else {
                viewHolder.itemView.favorite_image.setImageResource(R.drawable.ic_not_favorite)
                viewHolder.itemView.favorite_image.setImageColor(context)
                for (index in 0 until listOfFavoriteCurrencies.length()) {
                    val currencyCode =
                        listOfFavoriteCurrencies.getJSONObject(index).getCurrencyCode()
                    if (currencyCode == currencyData.currencyCode) {
                        listOfFavoriteCurrencies.remove(index)
                        File("$filesDir/${FavoritesActivity.favoriteCurrenciesListFileName}").writeText(
                            listOfFavoriteCurrencies.toString()
                        )
                        break
                    }
                }
            }
        }
    }
}
