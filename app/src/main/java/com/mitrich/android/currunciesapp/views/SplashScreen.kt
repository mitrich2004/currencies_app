package com.mitrich.android.currunciesapp.views

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mitrich.android.currunciesapp.activities.FavoritesActivity

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this, FavoritesActivity::class.java))
        finish()
    }
}
